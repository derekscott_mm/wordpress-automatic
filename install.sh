#!/usr/bin/env bash
#
#            ____   _____________.                             
#           `\   \ /   \______   |______`____  .______ ______.
#             \   Y   / |     ___\_  ___\ __ \ /  ___//  ___/
#             .\     / `|    |    |  | \  ___/.\___  \\___ \` 
#               \___/   |____|   .|__|  \___  /____ _//___  >
#                                     .     \/     `      \/. 
#
#  @details Installation script to get you up and running with a CLI infused
#           vagrant backed containerized wordpress installation. Other features
#           include Composer dependency management, and automated deployement. 
#


# Vagrant Variables
# ----------------------------------------------------------------------
BOXNAME="ds_myriad/fortress"
VPRESSZIP="https://bitbucket.org/derekscott_mm/wordpress-automatic/downloads/myriad_wp.zip"


# Script Variables
# ----------------------------------------------------------------------
vVPress="0.1.5";
scriptPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName=$(basename $0)                      # Set Script Name variable
scriptBasename="$(basename ${scriptName} .sh)" # Strips '.sh' from scriptName

# COLOR SETUP
if tput setaf 1&>/dev/null;then BLK="$(tput setaf 8)";RED="$(tput setaf 9)";GRN="$(tput setaf 10)";YLW="$(tput setaf 11)";BLU="$(tput setaf 12)";PUR="$(tput setaf 13)";CYN="$(tput setaf 14)";WHT="$(tput setaf 15)";B_BLK="$(tput setab 8)";B_RED="$(tput setab 124)";B_GRN="$(tput setab 10)";B_YLW="$(tput setab 11)";B_BLU="$(tput setab 12)";B_PUR="$(tput setab 13)";B_CYN="$(tput setab 14)";NORMAL="$(tput sgr0)";BOLD="$(tput bold)";else BLK="\e[1;30m";RED="\e[1;31m";GRN="\e[1;32m";YLW="\e[1;33m";BLU="\e[1;34m";PUR="\e[1;35m";CYN="\e[1;36m";WHT="\e[1;37m";B_BLK="\e[40m";B_RED="\e[41m";B_GRN="\e[42m";B_YLW="\e[43m";B_BLU="\e[44m";B_PUR="\e[45m";B_CYN="\e[46m";B_WHT="\e[47m";NORMAL="\e[0m";BOLD="\e[1m";fi;

# Core Functions
# ----------------------------------------------------------------------
## Message Out
function GOOD {
  echo "";
  echo "${BOLD}${WHT}${B_GRN}[ OK ]${NORMAL}${GRN}  $@${NORMAL}";
  return 1;
}
function BAD {
  echo "";
  echo "${BOLD}${WHT}${B_RED}[ OK ]${NORMAL}${RED}  $@${NORMAL}";
  return 0;
}
function WHATEVS {
  echo "";
  echo "${BOLD}${WHT}${B_BLU}[ OK ]${NORMAL}${BLU}  $@${NORMAL}";
}
## User Inputs
function ask {
    local prompt default reply

    while true; do

        if [ "${2:-}" = "Y" ];then prompt="Y/n";default=Y;elif [ "${2:-}" = "N" ];then prompt="y/N";default=N;else prompt="y/n";default=;fi;echo -en "$1 [$prompt] \n";read reply</dev/tty;if [ -z "$reply" ];then reply=$default;fi

        USERASK=$reply
        
        # Check if the reply is valid
        case "$reply" in
            Y*|y*) return 0 ;;
            N*|n*) return 1 ;;
        esac

    done
}
function askDir {
    local prompt default reply

    while true; do

        if [ "${2:-}" = "Y" ];then prompt="?";default=Y;elif [ "${2:-}" = "N" ];then prompt="?";default=N;else prompt="?";default=;fi;echo -en "$1 [$prompt] \n";read reply</dev/tty;if [ -z "$reply" ];then reply=$default;fi

        RAW=$(echo $reply | sed 's/[^a-zA-Z]//g')
        USERASK=$(echo "$RAW" | tr '[:upper:]' '[:lower:]')
        case "$reply" in
            N*|n*) return 1 ;;
            *) return 0 ;;
        esac
    done
}
function rando {
  VVVAR="$(/usr/bin/openssl rand -base64 8)"
  VVAR=$(echo $VVVAR | sed 's/[^a-zA-Z]//g')
  VAR=$(echo "$VVAR" | tr '[:upper:]' '[:lower:]')
}
# Main Menu
function startup {
  echo ""
  echo "${WHT}   _______${CYN}_______       ";
  echo "${WHT}  |${BLK}${B_BLU}       ${NORMAL}${CYN}       |${YLW}   ||  ";
  echo "${WHT}  |${BLK}${B_BLU}     / ${NORMAL}${CYN} \  .  |${YLW}   ||  ";
  echo "${WHT}  |${BLK}${B_BLU}  |\/  ${NORMAL}${CYN}  \/|  |${YLW}   ||   ${WHT}${B_BLU} MYRIAD MOBILE ${NORMAL} ";
  echo "${WHT}  |${BLK}${B_BLU}  |____${NORMAL}${CYN}____|  |${YLW}   ||   ${WHT}${B_BLU} VPress v${vVPress} ${NORMAL}";
  echo "${WHT}   \\${BLK}${B_BLU}      ${NORMAL}${CYN}      /${YLW}    ||    ";
  echo "${WHT}    \\${B_BLU}_____${NORMAL}${CYN}_____/${YLW}     ||  ${NORMAL}";
}



# Let's get this party started
# ----------------------------------------------------------------------
USERASK=""
VAR=""
DIRNAME=""

startup

echo ""
echo "${BGRN}Getting started. We need a folder."
if askDir "Enter a name or hit 'N' to generate one." N; then
  echo "Okay great choice! Using ${USERASK}";
  echo "";
else
  rando
  USERASK=$VAR
  echo "Randomizing.....";
  echo "Installing to: ${B_YLW}${BLK}  ${USERASK}  ${NORMAL}";
  echo "";
fi


mkdir $scriptPath/$USERASK
if [ -d "$scriptPath/$USERASK" ]; then
  cd $USERASK
else
  BAD "FATAL ERROR! DIRECTORY NOT CREATED"
  echo "Could not create:"
  echo $scriptPath/$USERASK
  echo "Please adjust script and/or directrory permissions."
  exit 0;
fi



# Install Requirements [ brew, vagrant, virtualbox ]
# ----------------------------------------------------------------------
if ! type "brew" > /dev/null; then
  WHATEVS "Installing Homebrew . . .";
  ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)";
else
  GOOD "Homebrew already installed";
fi

# Check for Vagrant + Virtualbox
if command -v vagrant >/dev/null 2>&1 && command -v virtualbox >/dev/null 2>&1 ; then
  GOOD "Vagrant + Virtualbox installed";
  echo ""
else
  brew tap phinze/homebrew-cask && brew install brew-cask;
  # Vagrant
  command -v vagrant >/dev/null 2>&1 || { WHATEVS "Brewing Vagrant . . ."; brew cask install vagrant; }
  # Virtualbox
  command -v virtualbox >/dev/null 2>&1 || { WHATEVS "Brewing Virtualbox . . ."; brew cask install virtualbox; }
fi

if [! command -v vagrant >/dev/null 2>&1 ] || [ ! command -v virtualbox >/dev/null 2>&1 ] ; then
  BAD "Something went wrong. Vagrant and/or Virtualbox were not installed.";
  echo -e "\n\n\t You will need to manually correct this error and rerun the script again.\n"
  echo -e "\t VIRTUALBOX: https://www.virtualbox.org/ \n"
  echo -e "\t VAGRANT:    http://www.vagrantup.com/ \n"
fi



# Setup Vagrant to manage Virtual Hosts
# ----------------------------------------------------------------------
# Default to No if the user presses enter without giving an answer:
if ask "Install the vagrant-hostsupdater plugin? [Optional]" N; then
  WHATEVS "Installing vagrant-hostsupdater"
  vagrant plugin install vagrant-hostsupdater
fi

WHATEVS "Downloading Vagrant Box - ${BOXNAME} . . ."
echo "${RED}-----------------------------------------${NORMAL}"
vagrant box add $BOXNAME

WHATEVS "Downloading VPress package . . ."
wget -O vpress.zip -o /dev/null $VPRESSZIP

WHATEVS "UNZIPPING AND PROCESSING . . ."
unzip -q ./vpress.zip

# Move into place and cleanup the mess
mv ./myriad_wp/* ./
mv ./myriad_wp/.editorconfig ./
mv ./myriad_wp/.gitignore  ./
rm ./vpress.zip
rm -R ./myriad_wp/
rm -R ./__MACOSX

echo "${RED}-----------------------------------------${NORMAL}"

GOOD "[F]ortified Wordpress Install [ OK ]"
echo ""


# Final Touches. Where do we go from here?
# ----------------------------------------------------------------------
# Default to No if the user presses enter without giving an answer:
if ask "Bring Vagrant box online? [vagrant up]" Y; then
  echo ""
  echo "     ${CYN}o${YLW}xxxx||${WHT}======================>      " && echo "${NORMAL}"
  WHATEVS "Running vagrant up command"
  vagrant up
else
  GOOD "Install completed. Exiting Script."
  echo -e "\nRun 'vagrant up' from this directory when you're not busy.\n"
  echo ""
  echo "     ${CYN}o${YLW}xxxx||${WHT}======================>      " && echo "${NORMAL}"
fi

exit 0;